﻿namespace WebApplication2
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Employees { get; set; }
    }
}
