﻿namespace WebApplication2
{
    public class Person
    {
        public string Name { get; set; } = "";
        public int Age { get; set; }
        public List<Language>? Languages { get; set; }
    }
}
