
using WebApplication2;


var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

builder.Configuration.AddXmlFile("google.xml", optional: false, reloadOnChange: true);

var google = new Company();
app.Configuration.Bind(google);

builder.Configuration.AddJsonFile("apple.json", optional: false, reloadOnChange: true);

var apple = new Company();
app.Configuration.Bind(apple);

builder.Configuration.AddIniFile("microsoft.ini", optional: false, reloadOnChange: true);

var microsoft = new Company();
app.Configuration.Bind(microsoft);


builder.Configuration.AddJsonFile("person.json", optional: false, reloadOnChange: true);
var person = new Person();

app.Configuration.Bind(person);


app.Use(async (context, next) =>
{
    context.Items["companyNameWithMostEmployees"] = new List<Company>{ google, microsoft, apple }.MaxBy((company)=> company.Employees)?.Name;
    
    await next.Invoke(context);
});

app.MapGet("/", (context) =>
{
    string companyNameWithHighestEmployees = (string?)context.Items["companyNameWithMostEmployees"] ?? "";
    return context.Response.WriteAsJsonAsync(companyNameWithHighestEmployees);
});

app.MapGet("/user", (context) =>
{
    return context.Response.WriteAsJsonAsync(person);
});


app.Run();
