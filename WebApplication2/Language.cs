﻿namespace WebApplication2
{
    public class Language
    {
        public string Name { get; set; } = "";
        public string Proficiency { get; set; } = "";
    }

    public enum Proficiency
    {
        A1,
        A2,
        B1,
        B2,
        C1,
        C2
    }
}
